(require 'org)
(org-babel-load-file
 (expand-file-name "settings.org"
                   user-emacs-directory))


;;; Auto generated code ;;;

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (yasnippet-snippets rust-mode multi-web-mode lsp-dart helm-ls-git helm yasnippet whitespace-cleanup-mode which-key wgrep visual-regexp string-edit smartparens simple-httpd ripgrep restclient request projectile prodigy perspective paredit nodejs-repl move-text markdown-mode inflections hydra htmlize highlight-escape-sequences groovy-mode gist flycheck-pos-tip flycheck fill-column-indicator f elisp-slime-nav dockerfile-mode deadgrep diff-hl css-eldoc
                        (css-eldoc)
                        (css-eldoc)
                        magit
                        (css-eldoc diff-hl deadgrep dockerfile-mode elisp-slime-nav f fill-column-indicator flycheck flycheck-pos-tip gist groovy-mode highlight-escape-sequences htmlize hydra inflections magit markdown-mode move-text nodejs-repl paredit perspective prodigy projectile request restclient ripgrep simple-httpd smartparens string-edit visual-regexp wgrep which-key whitespace-cleanup-mode yasnippet)
                        (css-eldoc)
                        yaml-mode typescript-mode))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )


